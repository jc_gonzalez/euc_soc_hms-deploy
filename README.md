Euclid SOC HMS Subsystem Deployment
========================================

## Installation

Select the base folder for your deployment.  It is strongly encouraged that this is 
set to `$HOME/SOC`. Set the variable `BASE_PATH` to this folder, and move into it:

    $ export BASE_PATH=$HOME/SOC
    $ cd $BASE_PATH
    
It is recommended that this is added to one of the scripts executed at log-in, like 
`.bash_profile`.

In addition, you will need to set the env. variable `GITBASE`, with the root of the 
GIT repository URLs.  It is recommended that this is set together with the `BASE_PATH` above:

    $ export GITBASE=https://....

Then, ensure you clone this repository (the one where the file you are just reading 
is located) with the name `hms`.  You could have done this just at cloning time:

    $ git clone --recurse-submodules ${GITBASE}/euc_soc_hms-deploy.git hms

Then, in the `hms` folder, launch the script `deploy.sh`:

    $ cd hms
    $ ./deploy.sh

## Execution

Enter the directory `$BASE_PATH/euc_soc_env`, and `source` the script `env.src`.

    $ cd $BASE_PATH/euc_soc_env
    $ source env.src

and then move the `hms` folder, and launch the script `launch-hms.sh`:

    $ cd ../hms
    $ ./launch-hms.sh
    
or, even better, launch it the background, like this:

    $ nohup ./launch-hms.sh &
    

