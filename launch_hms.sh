#!/bin/bash 
#------------------------------------------------------------------------------------
# launch-hms.sh
# Launcher for HMS IO simulator
#
# Created by J C Gonzalez <jcgonzalez@sciops.esa.int>
# Copyright (C) 2015-2020 by Euclid SOC Team
#------------------------------------------------------------------------------------
if [ -z "${BASE_PATH}" ]; then
    echo "You should set the env.var. BASE_PATH"
    exit 1
fi

SCRIPTPATH=$(cd $(dirname $0); pwd; cd - > /dev/null)
export PYTHONPATH=${BASE_PATH}/hms
SUBSYS=${BASE_PATH}/hms

rm -rf *.log

# In the following section we define the actions that take place for each of the
# data flows.  We group them in Uplink and Downlink sections.

##============================================================
## Prepare actions in io/out
##============================================================

##---- Uplink chain

# -None-

##---- Downlink chain

#-- HKTM products to be pushed to SIS
HKTM_HMS_TO_SIS_DIR=${SUBSYS}/io/out/edds
HKTM_HMS_TO_SIS_DEST=$SISUSER@$SISHOST:$SISBASE/soc/hms/in/hktm
mkdir -p ${HKTM_HMS_TO_SIS_DIR}
cat <<-EOF >${HKTM_HMS_TO_SIS_DIR}/actions.json
{
    "last_update": "",
    "history": [ "Initial creation" ],
    "actions": [ { "id": "send_hktm_prod", "type": "cmd",
                   "command": "$TRANSFER", "args": "{file_name} $HKTM_HMS_TO_SIS_DEST"} ]
}
EOF

#-- LE1 Enhanced products to be pushed to SIS
LE1_HMS_TO_SIS_DIR=${SUBSYS}/io/out/le1
LE1_HMS_TO_SIS_DEST=$SISUSER@$SISHOST:$SISBASE/soc/hms/in/le1e
mkdir -p ${LE1_HMS_TO_SIS_DIR}
cat <<-EOF >${LE1_HMS_TO_SIS_DIR}/actions.json
{
    "last_update": "",
    "history": [ "Initial creation" ],
    "actions": [ { "id": "send_le1enh", "type": "cmd",
                   "command": "$TRANSFER", "args": "{file_name} $LE1_HMS_TO_SIS_DEST" } ]
}
EOF

##============================================================
## Local and remote folders polling
##============================================================

WATCH="$PYTHON ${BASE_PATH}/lib/euc_soc_pytools/watch_folder/watch_folder.py"

# HKTM files to SOC[SIS]
$WATCH \
    -D ${HKTM_HMS_TO_SIS_DIR} \
    -l ${SUBSYS}/hms_hktm.log $SHOW_LOG_DEBUG &

# LE1 enh files to SOC[SIS]
$WATCH \
    -D ${LE1_HMS_TO_SIS_DIR} \
    -l ${SUBSYS}/hms_le1e.log $SHOW_LOG_DEBUG &

##============================================================
## Log monitoring
##============================================================

sleep 1

LOGS=$(echo ${SUBSYS}/hms_{sim,edds,hktm,le1,le1e,qla}.log)
touch $LOGS

#$SHOW_LOGS $LOGS && \
tail -f $LOGS && \
kill -- -$$
