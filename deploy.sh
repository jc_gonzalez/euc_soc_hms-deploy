#!/bin/bash 
#------------------------------------------------------------------------------------
# deploy.sh
# Retrieves the set of packages needed for the execution of the subsystem, and
# prepares the additional environment variables
#
# Created by J C Gonzalez <jcgonzalez@sciops.esa.int>
# Copyright (C) 2015-2020 by Euclid SOC Team
#------------------------------------------------------------------------------------
if [ -z "${BASE_PATH}" ]; then
    echo "You should set the env.var. BASE_PATH"
    exit 1
fi

SCRIPTPATH=$(cd $(dirname $0); pwd; cd - > /dev/null)

export PYTHONPATH=${BASE_PATH}/hms
SUBSYS=${BASE_PATH}/hms

rm -rf ${SUBSYS}/*.log

# Clone the repositories

cd ${BASE_PATH}
git clone --recurse-submodules ${GITBASE}/euc_soc_env.git

#cd ${SUBSYS}
#git clone --recurse-submodules ${GITBASE}/...

mkdir -p ${BASE_PATH}/lib
cd ${BASE_PATH}/lib

ADD_ENV_FILE="${BASE_PATH}/euc_soc_env/env.additional"
cat /dev/null > ${ADD_ENV_FILE}

repoList="euc_soc_pytools euc_soc_hktmgen euc_soc_le1 euc_soc_qlarptflat euc_soc_ossflat euc_soc_hms"
for repo in $repoList ; do
    git clone  --recurse-submodules ${GITBASE}/${repo}.git
    echo "export PATH=${BASE_PATH}/lib/${repo}:\$PATH" >> ${ADD_ENV_FILE}
done

echo "export PATH=.:\$PATH" >> ${ADD_ENV_FILE}

cd ${SUBSYS}
